#ifndef __G_DIGICAM_SCHED_POOL_H__
#define __G_DIGICAM_SCHED_POOL_H__

#include <gst/gst.h>

G_BEGIN_DECLS

/* --- standard type macros --- */
#define G_DIGICAM_TYPE_SCHED_POOL             (_g_digicam_sched_pool_get_type ())
#define G_DIGICAM_SCHED_POOL(pool)            (G_TYPE_CHECK_INSTANCE_CAST ((pool), G_DIGICAM_TYPE_SCHED_POOL, GDigicamSchedPool))
#define G_DIGICAM_IS_SCHED_POOL(pool)         (G_TYPE_CHECK_INSTANCE_TYPE ((pool), G_DIGICAM_TYPE_SCHED_POOL))
#define G_DIGICAM_SCHED_POOL_CLASS(pclass)    (G_TYPE_CHECK_CLASS_CAST ((pclass), G_DIGICAM_TYPE_SCHED_POOL, GDigicamSchedPoolClass))
#define G_DIGICAM_IS_SCHED_POOL_CLASS(pclass) (G_TYPE_CHECK_CLASS_TYPE ((pclass), G_DIGICAM_TYPE_SCHED_POOL))
#define G_DIGICAM_SCHED_POOL_GET_CLASS(pool)  (G_TYPE_INSTANCE_GET_CLASS ((pool), G_DIGICAM_TYPE_SCHED_POOL, GDigicamSchedPoolClass))
#define G_DIGICAM_SCHED_POOL_CAST(pool)       ((GDigicamSchedPool*)(pool))

typedef struct _GDigicamSchedPool GDigicamSchedPool;
typedef struct _GDigicamSchedPoolClass GDigicamSchedPoolClass;


/**
 * GDigicamSchedPool:
 * @parent_instance: Parent instance data.
 *
 * The #GDigicamSchedPool structure contains only private data and
 * should not be accessed directly.
 *
 * Its a subclass of #GstTaskPool.
 */
struct _GDigicamSchedPool {

    /*< private >*/
    GstTaskPool parent_instance;
};


/**
 * GDigicamSchedPoolClass:
 * @parent_class: Parent class data.
 *
 * Base class for #GDigicamSchedPool.
 */
struct _GDigicamSchedPoolClass {

    /*< private >*/
    GstTaskPoolClass parent_class;
};


GType            _g_digicam_sched_pool_get_type   (void);

GstTaskPool *    g_digicam_sched_pool_new         (void);
void g_digicam_sched_pool_rt_identity ();
void g_digicam_sched_pool_reg_identity ();

G_END_DECLS

#endif /* __G_DIGICAM_SCHED_POOL_H__ */
