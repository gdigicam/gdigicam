/*
 * This file is part of GDigicam
 *
 * Copyright (C) 2008-2009 Nokia Corporation and/or its
 * subsidiary(-ies).
 *
 * Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */


#include <stdlib.h>
#include <string.h>
#include <check.h>
#include <gst/gst.h>

#include "test_suites.h"
#include "check-utils.h"
#include "gdigicam-util.h"
#include "gdigicam-manager.h"
#include "gdigicam-sched-pool.h"

#include <libintl.h>

#define _(String) dgettext ("gdigicam", String)


static GstTaskPool *pool = NULL;


/* -------------------- Fixtures -------------------- */


static void
fx_setup_g_digicam (void)
{
    int argc = 0;
    char **argv = NULL;

    /* Initialize the GDigicam. */
    g_digicam_init (&argc, &argv);
}


static void
fx_setup_sched_pool (void)
{
    fx_setup_g_digicam ();

    pool = g_digicam_sched_pool_new ();
}


static void
fx_teardown_sched_pool (void)
{
    /* Unref pool  */
    if (NULL != pool) {
        g_object_unref (G_OBJECT (pool));
        pool = NULL;
    }
}



/* -------------------- Test cases -------------------- */


/* ----- Test case for new -----*/


/**
 * Purpose: test #GDigicamSchedPool class instantation.
 * Cases considered:
 *    - Create a #GDigicamSchedPool implementator object.
 */
START_TEST (test_g_digicam_sched_pool_new)
{
    /* Case 1. */
    pool = g_digicam_sched_pool_new ();

    /* Check that the gdigicam-sched-pool object has been created
     * properly. */
    fail_if (!G_DIGICAM_IS_SCHED_POOL (pool),
             "g-digicam-rt-pool: the returned object "
             "is not a GDigicamSchedPool instance.");
}
END_TEST


/* ----- Test case for rt_identity -----*/

/**
 * Purpose: test real time scheduling indentification
 * on #GDigicamSchedPool
 * Cases considered:
 *    - RT sched identity from a just created #GDigicamSchedPool.
 */
START_TEST (test_rt_identity)
{
    /* Test 1. */
    g_digicam_sched_pool_rt_identity ();
}
END_TEST


/* ----- Test case for reg_identity -----*/

/**
 * Purpose: test regular scheduling indentification
 * on #GDigicamSchedPool
 * Cases considered:
 *    - Regular identity from a just created #GDigicamSchedPool.
 */
START_TEST (test_reg_identity)
{
    /* Test 1. */
    g_digicam_sched_pool_reg_identity ();
}
END_TEST



/* ---------- Suite creation ---------- */


Suite *create_g_digicam_sched_pool_suite (void)
{
    /* Create the suite. */
    Suite *s = suite_create ("GDigicamSchedPool");

    /* Create test cases. */
    TCase *tc1 = tcase_create ("new");
    TCase *tc2 = tcase_create ("rt_identity");
    TCase *tc3 = tcase_create ("reg_identity");

    /* Create test case for new and add it to the suite. */
    tcase_add_checked_fixture (tc1, fx_setup_g_digicam, NULL);
    tcase_add_test (tc1, test_g_digicam_sched_pool_new);
    suite_add_tcase (s, tc1);

    /* Create test case for rt_identity and add it to the suite. */
    tcase_add_checked_fixture (tc2,
                               fx_setup_sched_pool,
                               fx_teardown_sched_pool);
    tcase_add_test (tc2, test_rt_identity);
    suite_add_tcase (s, tc2);

    /* Create test case for reg_identity and add it to the suite. */
    tcase_add_checked_fixture (tc3,
                               fx_setup_sched_pool,
                               fx_teardown_sched_pool);
    tcase_add_test (tc3, test_reg_identity);
    suite_add_tcase (s, tc3);

    /* Return created suite. */
    return s;
}
