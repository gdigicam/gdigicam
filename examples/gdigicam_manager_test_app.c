/*
 * This file is part of GDigicam
 *
 * Copyright (C) 2008-2009 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

/* Includes */
#define _GNU_SOURCE
#include <stdlib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <fcntl.h>

#include <gtk/gtk.h>
#include <gst/gst.h>
#include <gst/gstbin.h>

/* #include <gdk/gdk.h> */
#include <gdk/gdkx.h>

#include "gst-camerabin/gdigicam-camerabin.h"
#include "gdigicam-util.h"
#include "gdigicam-manager.h"
#include "gdigicam-debug.h"

#ifdef GDIGICAM_PLATFORM_MAEMO
#include <hildon/hildon-window.h>
#include <hildon/hildon-app-menu.h>
#include <hildon/hildon-picker-button.h>
#include <hildon/hildon-touch-selector.h>
#include <hildon/hildon-banner.h>
#else
#include <gtk/gtk.h>
#endif

/* Define sources and sinks according to
 * running environment
 * NOTE: If you want to run the application
 * in ARM scratchbox, you have to change these*/
#ifdef __arm__
/* The device by default supports only
 * vl4l2src for camera and xvimagesink
 * for screen */
#define VIDEO_SINK "xvimagesink"
#else
#ifdef GDIGICAM_PLATFORM_MAEMO
/* These are for the X86 SDK. Xephyr doesn't
 * support XVideo extension, so the application
 * must use ximagesink. The video source depends
 * on driver of your Video4Linux device so this
 * may have to be changed */
#define VIDEO_SINK "ximagesink"
#else
#define VIDEO_SINK "xvimagesink"
#endif
#endif

#define STANDBY_STATE "Standby"
#define RUNNING_STATE ""
#define TEXT_COLUMN 0
#define ID_COLUMN 1
#define DATE_BUFFER 9
#define BENCH_PATH "/home/user/MyDocs/Benchmarking/"

#define MAX_ITER 5

static void _load_settings (void);
static void _setup_gdigicam_manager (void);
static void _set_new_camerabin (const gchar *videoenc);
static GstElement *_build_camerabin_component (const gchar *videoenc);
static gboolean _on_image_capture_done (GDigicamManager *digicam_manager,
                                        const gchar *fname,
                                        gpointer data);
static void _on_capture_start (GDigicamManager *digicam_manager, gpointer data);
static void _on_capture_end (GDigicamManager *digicam_manager, gpointer data);
static void _on_picture_got (GDigicamManager *digicam_manager, gpointer data);
static void _on_internal_error (GDigicamManager *digicam_manager, gpointer data);
static void _on_image_preview (GDigicamManager *digicam_manager,
                               GdkPixbuf *preview,
                               gpointer data);
static void _load_default_settings (void);



typedef enum {
    DISABLED = 0,
    BENCHMARK1_TEST1,
    BENCHMARK1_TEST2,
    BENCHMARK1_TEST3,
    BENCHMARK2_TEST1,
    BENCHMARK2_TEST2,
    BENCHMARK2_TEST3,
    BENCHMARK3_TEST1,
    BENCHMARK3_TEST2,
    BENCHMARK3_TEST3,
    BENCHMARK4_TEST1,
    BENCHMARK4_TEST2,
    BENCHMARK4_TEST3,
    BENCHMARK5_TEST1,
    BENCHMARK5_TEST2,
    BENCHMARK5_TEST3,
    BENCHMARK6_TEST1,
    BENCHMARK6_TEST2,
    BENCHMARK6_TEST3,
} Benchmark;

typedef struct _GDigicamConf {
    GDigicamMode mode;
    GDigicamFlashmode flash;
    GDigicamWhitebalancemode wb;
    GDigicamExposuremode exp;
    gdouble exp_comp;
    GDigicamPreview preview;
    GDigicamResolution res;
    GDigicamAspectratio ar;
    gint iso;
    gdouble zoom;
    gboolean macro;
} GDigicamConf;

typedef struct _BenchmarkConf {
    Benchmark set1;
    Benchmark set2;
    Benchmark set3;
    Benchmark set4;
    Benchmark set5;
    Benchmark set6;
} BenchmarkConf;

static GDigicamConf *dcamConf = NULL;
static BenchmarkConf *benchConf = NULL;

static GtkWidget *mainwin = NULL;
static GtkWidget *viewfinder = NULL;
static GDigicamManager* digicam_manager = NULL;
static GtkWidget *status = NULL;
static gint colorkey = 0;
static GtkWidget *play_button = NULL;
static GtkWidget *stop_button = NULL;
static GtkWidget *testing_button = NULL;

static gboolean ready_for_new_capture = TRUE;
static gint test_iter = 0;

static gulong pd_id;
static gulong cs_id;
static gulong ce_id;
static gulong ip_id;
static gulong pg_id;


/* ******************************************************** */


static void
_done_clicked_cb (GtkWidget *button,
                  gpointer      data)
{
    gtk_dialog_response (GTK_DIALOG (data),
                         GTK_RESPONSE_OK);
}


static void
_cancel_clicked_cb (GtkWidget *button,
                    gpointer      data)
{
    gtk_dialog_response (GTK_DIALOG (data),
                         GTK_RESPONSE_CANCEL);
}

static void
_run_clicked_cb (GtkWidget *button,
                 gpointer      data)
{
    gtk_dialog_response (GTK_DIALOG (data),
                         GTK_RESPONSE_OK);
}

static gboolean
_closing (GtkWidget *widget,
          GdkEvent *event,
          gpointer user_data)
{
    gtk_main_quit();

    G_DIGICAM_DEBUG ("GDigicam example: Closing...\n\n\n\n");

    return FALSE;
}

static void
_play_button_clicked (GtkButton *button,
                      gpointer data)
{
    gulong xwindow_id = 0;
    GError *error = NULL;
    gboolean result;

    /* Set running state. */
    gtk_label_set_text (GTK_LABEL(status), RUNNING_STATE);

    /* Play bin */
    xwindow_id = GDK_WINDOW_XWINDOW (viewfinder->window);
    result = g_digicam_manager_play_bin(digicam_manager,
                                        xwindow_id,
                                        &error);
    if (result) {
        gtk_widget_set_sensitive (play_button, FALSE);
        gtk_widget_set_sensitive (stop_button, TRUE);
        gtk_widget_set_sensitive (testing_button, TRUE);

        G_DIGICAM_DEBUG("INFO: playing bin, window id: %d\n",
                        (int) xwindow_id);
    } else {
        G_DIGICAM_DEBUG("ERROR: %s", error->message);
    }
}

static void
_stop_button_clicked (GtkButton *button,
                      gpointer data)
{
    GError *error = NULL;
    gboolean result;

    /* Set running state. */
    gtk_label_set_text (GTK_LABEL(status), STANDBY_STATE);

    /* Set the Gstreamer bin to "stop" state */
    result = g_digicam_manager_stop_bin (digicam_manager,
                                         &error);
    if (result) {
        gtk_widget_set_sensitive (play_button, TRUE);
        gtk_widget_set_sensitive (stop_button, FALSE);
        gtk_widget_set_sensitive (testing_button, FALSE);
    } else {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("ERROR: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("ERROR: Internal error !!!\n");
        }
    }
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_flash_selector (void)
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Flash OFF. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_FLASHMODE_OFF,
                        TEXT_COLUMN, "Always Off",
                        -1);
    if (dcamConf->flash == G_DIGICAM_FLASHMODE_OFF) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Flash ON. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_FLASHMODE_ON,
                        TEXT_COLUMN, "Always On",
                        -1);
    if (dcamConf->flash == G_DIGICAM_FLASHMODE_ON) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Flash AUTO. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_FLASHMODE_AUTO,
                        TEXT_COLUMN, "Auto",
                        -1);
    if (dcamConf->flash == G_DIGICAM_FLASHMODE_AUTO) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Flash RED EYE REDUCTION. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_FLASHMODE_REDEYEREDUCTION,
                        TEXT_COLUMN, "Red Eyes",
                        -1);
    if (dcamConf->flash == G_DIGICAM_FLASHMODE_REDEYEREDUCTION) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_scene_selector ()
{
    GtkTreeIter iter;
    GtkListStore *model = NULL;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Scene AUTO. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_EXPOSUREMODE_AUTO,
                        TEXT_COLUMN, "Auto",
                        -1);
    if (dcamConf->exp == G_DIGICAM_EXPOSUREMODE_AUTO) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Scene NIGHT. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_EXPOSUREMODE_NIGHT,
                        TEXT_COLUMN, "Night",
                        -1);
    if (dcamConf->exp == G_DIGICAM_EXPOSUREMODE_NIGHT) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Scene SPORTS. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_EXPOSUREMODE_SPORTS,
                        TEXT_COLUMN, "Sports",
                        -1);
    if (dcamConf->exp == G_DIGICAM_EXPOSUREMODE_SPORTS) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Scene PORTRAIT. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_EXPOSUREMODE_PORTRAIT,
                        TEXT_COLUMN, "Portrait",
                        -1);
    if (dcamConf->exp == G_DIGICAM_EXPOSUREMODE_PORTRAIT) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Scene LANDSCAPE. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_EXPOSUREMODE_LANDSCAPE,
                        TEXT_COLUMN, "Landscape",
                        -1);
    if (dcamConf->exp == G_DIGICAM_EXPOSUREMODE_LANDSCAPE) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_wb_selector ()
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* White Balance AUTO. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_WHITEBALANCEMODE_AUTO,
                        TEXT_COLUMN, "Auto",
                        -1);
    if (dcamConf->wb == G_DIGICAM_WHITEBALANCEMODE_AUTO) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* White Balance SUNNY. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_WHITEBALANCEMODE_SUNLIGHT,
                        TEXT_COLUMN, "Sunny",
                        -1);
    if (dcamConf->wb == G_DIGICAM_WHITEBALANCEMODE_SUNLIGHT) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* White Balance CLOUDY. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_WHITEBALANCEMODE_CLOUDY,
                        TEXT_COLUMN, "Cloudy",
                        -1);
    if (dcamConf->wb == G_DIGICAM_WHITEBALANCEMODE_CLOUDY) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* White Balance TIGSTEN. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_WHITEBALANCEMODE_TUNGSTEN,
                        TEXT_COLUMN, "Tungsten",
                        -1);
    if (dcamConf->wb == G_DIGICAM_WHITEBALANCEMODE_TUNGSTEN) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* White Balance FLUORESCENT. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_WHITEBALANCEMODE_FLUORESCENT,
                        TEXT_COLUMN, "Fluorescent",
                        -1);
    if (dcamConf->wb == G_DIGICAM_WHITEBALANCEMODE_FLUORESCENT) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* White Balance FLASH. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_WHITEBALANCEMODE_FLASH,
                        TEXT_COLUMN, "Sunset",
                        -1);
    if (dcamConf->wb == G_DIGICAM_WHITEBALANCEMODE_FLASH) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }


    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_iso_selector ()
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Iso Value 0 (Auto). */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, 0,
                        TEXT_COLUMN, "Auto",
                        -1);
    if (dcamConf->iso == 0) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Iso Value 100. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, 100,
                        TEXT_COLUMN, "100",
                        -1);
    if (dcamConf->iso == -1) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Iso Value 200. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, 200,
                        TEXT_COLUMN, "200",
                        -1);
    if (dcamConf->iso == 200) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Iso Value 400. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, 400,
                        TEXT_COLUMN, "400",
                        -1);
    if (dcamConf->iso == 400) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_res_selector ()
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Resolution HIGH. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_RESOLUTION_HIGH,
                        TEXT_COLUMN, "High",
                        -1);
    if (dcamConf->res == G_DIGICAM_RESOLUTION_HIGH) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Resolution MEDIUM. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_RESOLUTION_MEDIUM,
                        TEXT_COLUMN, "Med",
                        -1);
    if (dcamConf->res == G_DIGICAM_RESOLUTION_MEDIUM) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Resolution LOW. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_RESOLUTION_LOW,
                        TEXT_COLUMN, "Low",
                        -1);
    if (dcamConf->res == G_DIGICAM_RESOLUTION_LOW) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_preview_selector ()
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Preview Enabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, 1,
                        TEXT_COLUMN, "Yes",
                        -1);
    if (dcamConf->preview == TRUE) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Preview disabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, G_DIGICAM_RESOLUTION_MEDIUM,
                        TEXT_COLUMN, "No",
                        -1);
    if (dcamConf->preview == FALSE) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_flash_selector_changed (HildonTouchSelector *sel,
                            gint col)
#else
_on_flash_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    dcamConf->flash = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_scene_selector_changed (HildonTouchSelector *sel,
                            gint col)
#else
_on_scene_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update scene param. */
    dcamConf->exp = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_wb_selector_changed (HildonTouchSelector *sel,
                         gint col)
#else
_on_wb_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    dcamConf->wb = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_iso_selector_changed (HildonTouchSelector *sel,
                          gint col)
#else
_on_iso_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    dcamConf->iso = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_res_selector_changed (HildonTouchSelector *sel,
                          gint col)
#else
_on_res_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    dcamConf->res = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_preview_selector_changed (HildonTouchSelector *sel,
                            gint col)
#else
_on_preview_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    dcamConf->preview = (gboolean) val;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static GtkWidget *
_new_picker_button (const gchar *title,
                    GtkSizeGroup *title_sg,
                    GtkSizeGroup *value_sg,
                    HildonTouchSelector *sel)
{
    GtkWidget *btn = NULL;
    HildonButton *h_btn = NULL;
    HildonPickerButton *pck_btn = NULL;
    HildonSizeType size;
    HildonButtonArrangement argmt;
    gchar *current = NULL;

    size = HILDON_SIZE_FINGER_HEIGHT;
    argmt = HILDON_BUTTON_ARRANGEMENT_HORIZONTAL;

    btn = hildon_picker_button_new (size, argmt);
    h_btn = HILDON_BUTTON (btn);
    pck_btn = HILDON_PICKER_BUTTON (btn);

    /* Create button and set selector. */
    hildon_button_set_title (h_btn, title);
    hildon_button_add_title_size_group (h_btn, title_sg);
    hildon_button_add_value_size_group (h_btn, value_sg);
    hildon_picker_button_set_selector (pck_btn, sel);

    /* Set current conf value form selector. */
    current = hildon_touch_selector_get_current_text (sel);
    hildon_button_set_value (h_btn, current);

    /* Free. */
    g_free (current);

    return btn;
}
#else
static GtkWidget *
_new_picker_button (const gchar *title,
                    GtkSizeGroup *title_sg,
                    GtkSizeGroup *value_sg,
                    GtkComboBox *sel)
{
    GtkWidget *hbox = NULL;
    GtkWidget *label = NULL;

    /* Create button and set selector. */
    hbox = gtk_hbox_new (FALSE, 0);
    label = gtk_label_new (title);
    gtk_box_pack_start (GTK_BOX (hbox), label,
                        FALSE, FALSE, 6);
    gtk_box_pack_end (GTK_BOX (hbox), GTK_WIDGET (sel),
                      TRUE, TRUE, 6);

    return hbox;
}
#endif


#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_set1_selector (void)
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Disabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, DISABLED,
                        TEXT_COLUMN, "Disabled",
                        -1);
    if (benchConf->set1 == DISABLED) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 1. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK1_TEST1,
                        TEXT_COLUMN, "Test 1",
                        -1);
    if (benchConf->set1 == BENCHMARK1_TEST1) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 2. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK1_TEST2,
                        TEXT_COLUMN, "Test 2",
                        -1);
    if (benchConf->set1 == BENCHMARK1_TEST2) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 3. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK1_TEST3,
                        TEXT_COLUMN, "Test 3",
                        -1);
    if (benchConf->set1 == BENCHMARK1_TEST3) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_set2_selector (void)
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Disabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, DISABLED,
                        TEXT_COLUMN, "Disabled",
                        -1);
    if (benchConf->set2 == DISABLED) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 1. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK2_TEST1,
                        TEXT_COLUMN, "Test 1",
                        -1);
    if (benchConf->set2 == BENCHMARK2_TEST1) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 2. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK2_TEST2,
                        TEXT_COLUMN, "Test 2",
                        -1);
    if (benchConf->set2 == BENCHMARK2_TEST2) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 3. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK2_TEST3,
                        TEXT_COLUMN, "Test 3",
                        -1);
    if (benchConf->set2 == BENCHMARK2_TEST3) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}


#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_set3_selector (void)
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Disabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, DISABLED,
                        TEXT_COLUMN, "Disabled",
                        -1);
    if (benchConf->set3 == DISABLED) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 1. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK3_TEST1,
                        TEXT_COLUMN, "Test 1",
                        -1);
    if (benchConf->set3 == BENCHMARK3_TEST1) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 2. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK3_TEST2,
                        TEXT_COLUMN, "Test 2",
                        -1);
    if (benchConf->set3 == BENCHMARK3_TEST2) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 3. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK3_TEST3,
                        TEXT_COLUMN, "Test 3",
                        -1);
    if (benchConf->set3 == BENCHMARK3_TEST3) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_set4_selector (void)
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Disabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, DISABLED,
                        TEXT_COLUMN, "Disabled",
                        -1);
    if (benchConf->set4 == DISABLED) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 1. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK4_TEST1,
                        TEXT_COLUMN, "Test 1",
                        -1);
    if (benchConf->set4 == BENCHMARK4_TEST1) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 2. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK4_TEST2,
                        TEXT_COLUMN, "Test 2",
                        -1);
    if (benchConf->set4 == BENCHMARK4_TEST2) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 3. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK4_TEST3,
                        TEXT_COLUMN, "Test 3",
                        -1);
    if (benchConf->set4 == BENCHMARK4_TEST3) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_set5_selector (void)
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Disabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, DISABLED,
                        TEXT_COLUMN, "Disabled",
                        -1);
    if (benchConf->set5 == DISABLED) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 1. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK5_TEST1,
                        TEXT_COLUMN, "Test 1",
                        -1);
    if (benchConf->set5 == BENCHMARK5_TEST1) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 2. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK5_TEST2,
                        TEXT_COLUMN, "Test 2",
                        -1);
    if (benchConf->set5 == BENCHMARK5_TEST2) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 3. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK5_TEST3,
                        TEXT_COLUMN, "Test 3",
                        -1);
    if (benchConf->set5 == BENCHMARK5_TEST3) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonTouchSelector *
#else
static GtkComboBox *
#endif
_create_set6_selector (void)
{
    GtkListStore *model = NULL;
    GtkTreeIter iter;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
    HildonTouchSelectorColumn *col = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    /* Create selector. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    sel = HILDON_TOUCH_SELECTOR (hildon_touch_selector_new ());
#else
    sel = GTK_COMBO_BOX (gtk_combo_box_entry_new ());
#endif

    /* Create model and selector column. */
    model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
#ifdef GDIGICAM_PLATFORM_MAEMO
    col = hildon_touch_selector_append_text_column (sel,
                                                    GTK_TREE_MODEL (model),
                                                    TRUE);
    hildon_touch_selector_set_column_selection_mode (sel,
                                                     HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
    g_object_set (col, "text-column", TEXT_COLUMN, NULL);
#else
    gtk_combo_box_set_model (sel, GTK_TREE_MODEL (model));
    gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(sel), 0);
#endif

    /* Disabled. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, DISABLED,
                        TEXT_COLUMN, "Disabled",
                        -1);
    if (benchConf->set6 == DISABLED) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 1. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK6_TEST1,
                        TEXT_COLUMN, "Test 1",
                        -1);
    if (benchConf->set6 == BENCHMARK6_TEST1) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 2. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK6_TEST2,
                        TEXT_COLUMN, "Test 2",
                        -1);
    if (benchConf->set6 == BENCHMARK6_TEST2) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Test 3. */
    gtk_list_store_append (model, &iter);
    gtk_list_store_set (model, &iter,
                        ID_COLUMN, BENCHMARK6_TEST3,
                        TEXT_COLUMN, "Test 3",
                        -1);
    if (benchConf->set6 == BENCHMARK6_TEST3) {
#ifdef GDIGICAM_PLATFORM_MAEMO
        hildon_touch_selector_select_iter (sel, 0,
                                           &iter, FALSE);
#else
        gtk_combo_box_set_active_iter (sel, &iter);
#endif
    }

    /* Free. */
    g_object_unref (model);

    return sel;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_set1_selector_changed (HildonTouchSelector *sel,
                           gint col)
#else
_on_set1_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    benchConf->set1 = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_set2_selector_changed (HildonTouchSelector *sel,
                           gint col)
#else
_on_set2_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    benchConf->set2 = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_set3_selector_changed (HildonTouchSelector *sel,
                           gint col)
#else
_on_set3_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    benchConf->set3 = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_set4_selector_changed (HildonTouchSelector *sel,
                           gint col)
#else
_on_set4_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    benchConf->set4 = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_set5_selector_changed (HildonTouchSelector *sel,
                           gint col)
#else
_on_set5_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    benchConf->set5 = val;
}

static void
#ifdef GDIGICAM_PLATFORM_MAEMO
_on_set6_selector_changed (HildonTouchSelector *sel,
                           gint col)
#else
_on_set6_selector_changed (GtkComboBox *sel)
#endif
{
    GtkTreeModel *model = NULL;
    GtkTreeIter iter;
    gint val;

    /* Get curent selected value. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    model = hildon_touch_selector_get_model (sel, col);
    hildon_touch_selector_get_selected (sel, col, &iter);
#else
    model = gtk_combo_box_get_model (sel);
    gtk_combo_box_get_active_iter (sel, &iter);
#endif
    gtk_tree_model_get (model, &iter, ID_COLUMN, &val, -1);

    /* Update flash param. */
    benchConf->set6 = val;
}


static GtkWidget *
_create_testing_dialog ()
{
    GtkWidget *btn = NULL;
    GtkDialog *dialog = NULL;
    GtkContainer *content = NULL;
    GtkTable *layout = NULL;
    GtkContainer *toolbar = NULL;
    GtkSizeGroup *tit = NULL;
    GtkSizeGroup *val = NULL;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    tit = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
    val = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
    content = GTK_CONTAINER (gtk_vbox_new (FALSE, 0));
    layout = GTK_TABLE (gtk_table_new (2, 3, TRUE));
    toolbar = GTK_CONTAINER (gtk_hbox_new (FALSE, 3));
    gtk_container_add (content, GTK_WIDGET (layout));
    gtk_box_pack_end (GTK_BOX (content), GTK_WIDGET (toolbar),
                      FALSE, FALSE, 6);

    dialog = GTK_DIALOG(gtk_dialog_new ());
    gtk_window_set_title (GTK_WINDOW (dialog), "Benchmarking");

    /* Testing benchmark 1 */
    sel = _create_set1_selector ();
    btn = _new_picker_button ("Set 1", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 0, 1, 0, 1);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_set1_selector_changed),
                      NULL);

    /* Testing benchmark 2 */
    sel = _create_set2_selector ();
    btn = _new_picker_button ("Set 2", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 1, 2, 0, 1);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_set2_selector_changed),
                      NULL);

    /* Testing benchmark 3 */
    sel = _create_set3_selector ();
    btn = _new_picker_button ("Set 3", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 0, 1, 1, 2);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_set3_selector_changed),
                      NULL);

    /* Testing benchmark 4 */
    sel = _create_set4_selector ();
    btn = _new_picker_button ("Set 4", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 1, 2, 1, 2);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_set4_selector_changed),
                      NULL);

    /* Testing benchmark 5 */
    sel = _create_set5_selector ();
    btn = _new_picker_button ("Set 5", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 2, 3, 0, 1);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_set5_selector_changed),
                      NULL);

    /* Testing benchmark 6 */
    sel = _create_set6_selector ();
    btn = _new_picker_button ("Set 6", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 2, 3, 1, 2);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_set6_selector_changed),
                      NULL);
    /* Dialog buttons. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    btn = hildon_button_new_with_text (HILDON_SIZE_FINGER_HEIGHT |
                                       HILDON_SIZE_AUTO_WIDTH,
                                       HILDON_BUTTON_ARRANGEMENT_HORIZONTAL,
                                       "  Run Tests ", NULL);
#else
    btn = gtk_button_new_with_label ("Done");
#endif
    gtk_box_pack_end (GTK_BOX (toolbar), btn,
                        FALSE, FALSE, 0);
    g_signal_connect (btn, "clicked",
                      G_CALLBACK (_run_clicked_cb),
                      dialog);
#ifdef GDIGICAM_PLATFORM_MAEMO
    btn = hildon_button_new_with_text (HILDON_SIZE_FINGER_HEIGHT |
                                       HILDON_SIZE_AUTO_WIDTH,
                                       HILDON_BUTTON_ARRANGEMENT_HORIZONTAL,
                                       "   Cancel   ", NULL);
#else
    btn = gtk_button_new_with_label ("Cancel");
#endif
    gtk_box_pack_end (GTK_BOX (toolbar), btn,
                        FALSE, FALSE, 0);
    g_signal_connect (btn, "clicked",
                      G_CALLBACK (_cancel_clicked_cb),
                      dialog);

    /* Show settings dialog internal widgets. */
    gtk_widget_show_all (GTK_WIDGET(content));
    gtk_container_add (GTK_CONTAINER(dialog->vbox),
                       GTK_WIDGET(content));

    return GTK_WIDGET(dialog);
}


static GtkWidget *
_create_settings_dialog ()
{
    GtkWidget *btn = NULL;
    GtkDialog *dialog = NULL;
    GtkContainer *content = NULL;
    GtkTable *layout = NULL;
    GtkContainer *toolbar = NULL;
    GtkSizeGroup *tit = NULL;
    GtkSizeGroup *val = NULL;
#ifdef GDIGICAM_PLATFORM_MAEMO
    HildonTouchSelector *sel = NULL;
#else
    GtkComboBox *sel = NULL;
#endif

    tit = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
    val = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
    content = GTK_CONTAINER (gtk_vbox_new (FALSE, 0));
    layout = GTK_TABLE (gtk_table_new (2, 3, TRUE));
    toolbar = GTK_CONTAINER (gtk_hbox_new (FALSE, 3));
    gtk_container_add (content, GTK_WIDGET (layout));
    gtk_box_pack_end (GTK_BOX (content), GTK_WIDGET (toolbar),
                      FALSE, FALSE, 6);

    dialog = GTK_DIALOG(gtk_dialog_new ());
    gtk_window_set_title (GTK_WINDOW (dialog), "Settings");

    /* Flash combo */
    sel = _create_flash_selector ();
    btn = _new_picker_button ("Flash", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 0, 1, 0, 1);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_flash_selector_changed),
                      NULL);

    /* Scene Mode combo */
    sel = _create_scene_selector ();
    btn = _new_picker_button ("Scene", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 1, 2, 0, 1);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_scene_selector_changed),
                      NULL);

    /* White Balance combo */
    sel = _create_wb_selector ();
    btn = _new_picker_button ("WB", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 0, 1, 1, 2);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_wb_selector_changed),
                      NULL);

    /* Iso Value combo */
    sel = _create_iso_selector ();
    btn = _new_picker_button ("ISO", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 1, 2, 1, 2);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_iso_selector_changed),
                      NULL);

    /* Resolution combo */
    sel = _create_res_selector ();
    btn = _new_picker_button ("Res", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 2, 3, 0, 1);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_res_selector_changed),
                      NULL);

    /* Preview combo */
    sel = _create_preview_selector ();
    btn = _new_picker_button ("Preview", tit, val, sel);
    gtk_table_attach_defaults (layout, btn, 2, 3, 1, 2);
    g_signal_connect (G_OBJECT (sel), "changed",
                      G_CALLBACK (_on_preview_selector_changed),
                      NULL);

    /* Dialog buttons. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    btn = hildon_button_new_with_text (HILDON_SIZE_FINGER_HEIGHT |
                                       HILDON_SIZE_AUTO_WIDTH,
                                       HILDON_BUTTON_ARRANGEMENT_HORIZONTAL,
                                       "    Done    ", NULL);
#else
    btn = gtk_button_new_with_label ("Done");
#endif
    gtk_box_pack_end (GTK_BOX (toolbar), btn,
                        FALSE, FALSE, 0);
    g_signal_connect (btn, "clicked",
                      G_CALLBACK (_done_clicked_cb),
                      dialog);
#ifdef GDIGICAM_PLATFORM_MAEMO
    btn = hildon_button_new_with_text (HILDON_SIZE_FINGER_HEIGHT |
                                       HILDON_SIZE_AUTO_WIDTH,
                                       HILDON_BUTTON_ARRANGEMENT_HORIZONTAL,
                                       "   Cancel   ", NULL);
#else
    btn = gtk_button_new_with_label ("Cancel");
#endif
    gtk_box_pack_end (GTK_BOX (toolbar), btn,
                        FALSE, FALSE, 0);
    g_signal_connect (btn, "clicked",
                      G_CALLBACK (_cancel_clicked_cb),
                      dialog);

    /* Show settings dialog internal widgets. */
    gtk_widget_show_all (GTK_WIDGET(content));
    gtk_container_add (GTK_CONTAINER(dialog->vbox),
                       GTK_WIDGET(content));

    return GTK_WIDGET(dialog);
}


static GDigicamConf *
_backup_conf (void)
{
    GDigicamConf * conf = g_slice_new0 (GDigicamConf);
    conf->mode    = dcamConf->mode;
    conf->res     = dcamConf->res;
    conf->ar      = dcamConf->ar;
    conf->flash   = dcamConf->flash;
    conf->iso     = dcamConf->iso;
    conf->wb      = dcamConf->wb;
    conf->exp     = dcamConf->exp;
    conf->exp     = dcamConf->exp;
    conf->preview = dcamConf->preview;
    conf->macro   = dcamConf->macro;
    conf->zoom    = dcamConf->zoom;

    return conf;
}

static BenchmarkConf *
_backup_benchmark_conf (void)
{
    BenchmarkConf * conf = g_slice_new0 (BenchmarkConf);
    conf->set1 = benchConf->set1;
    conf->set2 = benchConf->set2;
    conf->set3 = benchConf->set3;
    conf->set4 = benchConf->set4;
    conf->set5 = benchConf->set5;
    conf->set6 = benchConf->set6;

    return conf;
}

static void
_settings_button_clicked (GtkButton *button,
                          gpointer data)
{
    GtkWidget *dialog = NULL;
    GDigicamConf *conf = NULL;
    GDigicamConf *tmp = NULL;
    gint response = 0;

    /* Create settings dialog. */
    dialog = _create_settings_dialog ();

    /* Backup curent settings. */
    conf = _backup_conf ();

    /* Run dialog. */
    response = gtk_dialog_run (GTK_DIALOG(dialog));
    if (response == GTK_RESPONSE_OK) {
        /* Set the new settings. */
        tmp = conf;
        _load_settings ();
    } else {
        /* Restore previous conf form backup. */
        tmp = dcamConf;
        dcamConf = conf;
    }

    /* Free. */
    gtk_widget_destroy (dialog);
    g_slice_free (GDigicamConf, tmp);
}

static void
_reset_button_clicked (GtkButton *button,
                       gpointer data)
{
    /* Set device configuration parameters. */
    _load_default_settings ();
}

static gboolean
_get_still_picture (const gchar *path)
{
    GDigicamCamerabinPictureHelper *helper = NULL;
    GError *error = NULL;
    gboolean result;

    G_DIGICAM_DEBUG ("GDigicamTest:_benchmark_set1_test1:: "
                     "Capturing still picture ...");

    /* Create helper. */
    helper = g_slice_new (GDigicamCamerabinPictureHelper);
    helper->file_path = path;
    helper->metadata = NULL;

    /* Call GDigicam Manager to perform capture operation. */
    result = g_digicam_manager_capture_still_picture (digicam_manager,
                                                      helper->file_path,
                                                      &error,
                                                      helper);

    /* Checlk errors. */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("GDigicamTest:_benchmark_set1_test1:: "
                             "%s.", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("GDigicamTest:_benchmark_set1_test1:: "
                        "Internal error !!!");
        }
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinPictureHelper, helper);

    return result;
}

static gboolean
_create_benchmarking_results_store (const gchar *store)
{
    gboolean result = TRUE;

    /* Check if store already exists. */
    if (g_access (store, F_OK) == 0) return TRUE;

    /* Create directories (if necessary). */
    result = (g_mkdir_with_parents (store,
                                   S_IRWXU |
                                   S_IRGRP | S_IXGRP |
                                   S_IROTH | S_IXOTH) == 0);

    /* Check errors. */
    if (result) {
        G_DIGICAM_DEBUG ("GDigicamTest:_create_benchmarking_results_store:: "
                         "Created the directory: %s\n",
                         store);
    } else {
        G_DIGICAM_DEBUG ("GDigicamTest:_create_benchmarking_results_store:: "
                         "Error creating the directory: %s\n",
                         store);
    }

    return result;
}

static gboolean
_benchmark_set1_test1 ()
{
    const gchar *store = NULL;
    const gchar *format = NULL;
    gchar *path = NULL;
    struct tm* tm = NULL;
    time_t now;;
    gchar date[DATE_BUFFER];
    gboolean result = TRUE;

    /* Configure GDigicam Manager. */
    if (test_iter == 0) {
        pd_id = g_signal_connect (digicam_manager, "pict-done",
                                  (GCallback) _on_image_capture_done,
                                  mainwin);

        cs_id = g_signal_connect (digicam_manager, "capture-start",
                                  (GCallback) _on_capture_start,
                                  mainwin);

        ce_id = g_signal_connect (digicam_manager, "capture-end",
                                  (GCallback) _on_capture_end,
                                  mainwin);

        ip_id = g_signal_connect (digicam_manager, "image-preview",
                                  (GCallback) _on_image_preview,
                                  mainwin);

        pg_id = g_signal_connect (digicam_manager, "picture-got",
                                  (GCallback) _on_picture_got,
                                  mainwin);
        pd_id = g_signal_connect (digicam_manager, "internal-error",
                                  (GCallback) _on_internal_error,
                                  mainwin);
    } else if (test_iter > MAX_ITER) {
        g_signal_handler_disconnect (digicam_manager, pd_id);
        g_signal_handler_disconnect (digicam_manager, cs_id);
        g_signal_handler_disconnect (digicam_manager, ce_id);
        g_signal_handler_disconnect (digicam_manager, ip_id);
        g_signal_handler_disconnect (digicam_manager, pg_id);

        return TRUE;
    }

    /* Check date. */
    now = time (NULL);
    tm = localtime ((time_t *) &now);
    strftime (date, DATE_BUFFER, "%Y%m%d", tm);
    store = BENCH_PATH "Set1/Test1/";
    format = "%s/%s_%03d.%s";
    path = g_strdup_printf ("%s/%s_XXXX.jpg", store, date);

    /* Create directories (if necessary). */
    if (!_create_benchmarking_results_store (store)) {
        goto cleanup;
    }

    /* Execute test. */
    g_sprintf (path, format, store, date, ++test_iter, "jpg");
    _get_still_picture (path);

    /* Free. */
 cleanup:
    g_free (path);

    return result;
}



static void
_run_tests (void)
{
    /* Benchmarking using Set1 tests. */
    switch (benchConf->set1) {
    case DISABLED:
        G_DIGICAM_DEBUG ("Benchmarking Set1 tests disabled.");
        break;
    case BENCHMARK1_TEST1:
        test_iter = 0;
        _benchmark_set1_test1 ();
        break;
    case BENCHMARK1_TEST2:
        break;
    case BENCHMARK1_TEST3:
        break;
    default:
        g_assert_not_reached ();
    }

    /* Benchmarking using Set2 tests. */
    switch (benchConf->set2) {
    case DISABLED:
        G_DIGICAM_DEBUG ("Benchmarking Set2 tests disabled.");
        break;
    case BENCHMARK2_TEST1:
        break;
    case BENCHMARK2_TEST2:
        break;
    case BENCHMARK2_TEST3:
        break;
    default:
        g_assert_not_reached ();
    }

    /* Benchmarking using Set3 tests. */
    switch (benchConf->set3) {
    case DISABLED:
        G_DIGICAM_DEBUG ("Benchmarking Set3 tests disabled.");
        break;
    case BENCHMARK3_TEST1:
        break;
    case BENCHMARK3_TEST2:
        break;
    case BENCHMARK3_TEST3:
        break;
    default:
        g_assert_not_reached ();
    }

    /* Benchmarking using Set4 tests. */
    switch (benchConf->set4) {
    case DISABLED:
        G_DIGICAM_DEBUG ("Benchmarking Set4 tests disabled.");
        break;
    case BENCHMARK4_TEST1:
        break;
    case BENCHMARK4_TEST2:
        break;
    case BENCHMARK4_TEST3:
        break;
    default:
        g_assert_not_reached ();
    }

    /* Benchmarking using Set5 tests. */
    switch (benchConf->set5) {
    case DISABLED:
        G_DIGICAM_DEBUG ("Benchmarking Set5 tests disabled.");
        break;
    case BENCHMARK5_TEST1:
        break;
    case BENCHMARK5_TEST2:
        break;
    case BENCHMARK5_TEST3:
        break;
    default:
        g_assert_not_reached ();
    }

    /* Benchmarking using Set6 tests. */
    switch (benchConf->set6) {
    case DISABLED:
        G_DIGICAM_DEBUG ("Benchmarking Set6 tests disabled.");
        break;
    case BENCHMARK6_TEST1:
        break;
    case BENCHMARK6_TEST2:
        break;
    case BENCHMARK6_TEST3:
        break;
    default:
        g_assert_not_reached ();
    }
}

static void
_testing_button_clicked (GtkButton *button,
                            gpointer data)
{
    GtkWidget *dialog = NULL;
    BenchmarkConf *conf = NULL;
    BenchmarkConf *tmp = NULL;
    gint response;

    dialog = _create_testing_dialog ();

    /* Backup curent settings. */
    conf = _backup_benchmark_conf ();

    /* Run dialog. */
    response = gtk_dialog_run (GTK_DIALOG(dialog));
    if (response == GTK_RESPONSE_OK) {
        /* Set the new settings. */
        tmp = conf;
        _run_tests ();
    } else {
        /* Restore previous conf form backup. */
        tmp = benchConf;
        benchConf = conf;
    }

    /* Free. */
    gtk_widget_destroy (dialog);
    g_slice_free (BenchmarkConf, tmp);
}

#ifdef GDIGICAM_PLATFORM_MAEMO
static HildonAppMenu *
_create_app_menu (void)
{
    HildonAppMenu *menu = NULL;
    GtkWidget *button = NULL;

    menu = HILDON_APP_MENU (hildon_app_menu_new ());

    play_button = gtk_button_new_with_label ("Play");
    g_signal_connect (play_button, "clicked",
                      G_CALLBACK (_play_button_clicked),
                      NULL);
    hildon_app_menu_append (menu, GTK_BUTTON (play_button));

    stop_button = gtk_button_new_with_label ("Stop");
    gtk_widget_set_sensitive (stop_button, FALSE);
    g_signal_connect (stop_button, "clicked",
                      G_CALLBACK (_stop_button_clicked),
                      NULL);
    hildon_app_menu_append (menu, GTK_BUTTON (stop_button));

    button = gtk_button_new_with_label ("Settings");
    g_signal_connect (button, "clicked",
                      G_CALLBACK (_settings_button_clicked),
                      NULL);
    hildon_app_menu_append (menu, GTK_BUTTON (button));

    button = gtk_button_new_with_label ("Reset Settings");
    g_signal_connect (button, "clicked",
                      G_CALLBACK (_reset_button_clicked),
                      NULL);
    hildon_app_menu_append (menu, GTK_BUTTON (button));

    testing_button = gtk_button_new_with_label ("Testing Benchmark");
    gtk_widget_set_sensitive (testing_button, FALSE);
    g_signal_connect (testing_button, "clicked",
                      G_CALLBACK (_testing_button_clicked),
                      NULL);
    hildon_app_menu_append (menu, GTK_BUTTON (testing_button));

    gtk_widget_show_all (GTK_WIDGET(menu));

    /* Initial state. */
    gtk_widget_set_sensitive (play_button, TRUE);
    gtk_widget_set_sensitive (stop_button, FALSE);
    gtk_widget_set_sensitive (testing_button, FALSE);

    return menu;
}
#endif

static GtkWidget *
_create_main_window (void)
{
    GdkColor color;

#ifdef GDIGICAM_PLATFORM_MAEMO
    mainwin = hildon_window_new ();
#else
    mainwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_size_request (mainwin, 800, 480);
#endif
    gtk_window_set_title (GTK_WINDOW (mainwin),
                          "GDigicam-UI");

    /* Create viewfinder and set the colorkey. */
    color.pixel = 0;
    color.red = (colorkey & 0xff0000) >> 8;
    color.green = (colorkey & 0x00ff00);
    color.blue = (colorkey & 0x0000ff) << 8;
    viewfinder = gtk_event_box_new ();
    gtk_container_set_border_width (GTK_CONTAINER (viewfinder), 0);
    gtk_event_box_set_visible_window(GTK_EVENT_BOX(viewfinder), TRUE);
    g_object_set (viewfinder, "can-focus", TRUE, NULL);
    gtk_widget_modify_bg (GTK_WIDGET(viewfinder),
                          GTK_STATE_NORMAL,
                          &color);
    gtk_container_add (GTK_CONTAINER (mainwin), viewfinder);

#ifdef GDIGICAM_PLATFORM_MAEMO
    /* Create menu. */

    HildonAppMenu *menu = _create_app_menu ();
    hildon_window_set_app_menu (HILDON_WINDOW (mainwin),
                                HILDON_APP_MENU (menu));
#else
    GtkWidget *layout = NULL;
    GtkWidget *toolbar = NULL;
    GtkWidget *button = NULL;

    /* Create Layout.  */
    layout = gtk_hbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (viewfinder), layout);

    /* Create status indicator. */
    status = gtk_label_new (STANDBY_STATE);
    gtk_box_pack_start (GTK_BOX (layout), status,
                        TRUE, TRUE, 0);

    /* Create toolbar. */
    toolbar = gtk_vbox_new (FALSE, 0);
    gtk_box_pack_end (GTK_BOX (layout), toolbar,
                      FALSE, FALSE, 0);

    /* create toolbar items.  */
    play_button = gtk_button_new_with_label ("Play bin.");
    gtk_container_add (GTK_CONTAINER (toolbar), play_button);
    g_signal_connect (G_OBJECT (play_button), "clicked",
                      G_CALLBACK (_play_button_clicked),
                      NULL);
    stop_button = gtk_button_new_with_label ("Stop bin.");
    gtk_container_add (GTK_CONTAINER (toolbar), stop_button);
    g_signal_connect (G_OBJECT (stop_button), "clicked",
                      G_CALLBACK (_stop_button_clicked),
                      NULL);
    button = gtk_button_new_with_label ("Settings");
    gtk_container_add (GTK_CONTAINER (toolbar), button);
    g_signal_connect (G_OBJECT (button), "clicked",
                      G_CALLBACK (_settings_button_clicked),
                      NULL);
    button = gtk_button_new_with_label ("Reset settings");
    gtk_container_add (GTK_CONTAINER (toolbar), button);
    g_signal_connect (G_OBJECT (button), "clicked",
                      G_CALLBACK (_reset_button_clicked),
                      NULL);
    testing_button = gtk_button_new_with_label ("Testing Benchmark");
    gtk_container_add (GTK_CONTAINER (toolbar), testing_button);
    g_signal_connect (G_OBJECT (testing_button), "clicked",
                      G_CALLBACK (_testing_button_clicked),
                      NULL);

    /* Initial state. */
    gtk_widget_set_sensitive (play_button, TRUE);
    gtk_widget_set_sensitive (stop_button, FALSE);
    gtk_widget_set_sensitive (testing_button, FALSE);

#endif

    return mainwin;
}

int
main (int argc, char **argv)
{
    GtkWidget *mainwin = NULL;

    /* Initialize the GDigicam */
    g_digicam_init (&argc, &argv);

    /* Initialize i18n support */
    gtk_set_locale ();

    /* Initialize the GTK. */
    gtk_init(&argc, &argv);

    /* Setting up GDigicamManager instance */
    /* The colorkey hast to be set before creating */
    /* the main window. */
    _setup_gdigicam_manager ();

    /* Create the main window */
    mainwin = _create_main_window ();


    /* Connect signal to X in the upper corner */
    g_signal_connect (G_OBJECT (mainwin), "delete_event",
                      G_CALLBACK (_closing), NULL);

    /* Begin the main application */
    gtk_widget_show_all (mainwin);

    gtk_main ();

    /* Exit */
    return 0;
}

static void
_descriptor_set_capabilities (GDigicamDescriptor *descriptor)
{
    g_assert (NULL != descriptor);

    /* Setup 'camerabin' capabilities descriptor */
    descriptor->max_zoom_macro_enabled  = 6;
    descriptor->max_zoom_macro_disabled = 6;
    descriptor->max_digital_zoom        = 6;
    descriptor->supported_modes =
        descriptor->supported_modes |
        G_DIGICAM_MODE_STILL        |
        G_DIGICAM_MODE_VIDEO;
    descriptor->supported_iso_sensitivity_modes =
        descriptor->supported_iso_sensitivity_modes |
        G_DIGICAM_ISOSENSITIVITYMODE_MANUAL         |
        G_DIGICAM_ISOSENSITIVITYMODE_AUTO;
    descriptor->supported_white_balance_modes =
        descriptor->supported_white_balance_modes |
        G_DIGICAM_WHITEBALANCEMODE_MANUAL         |
        G_DIGICAM_WHITEBALANCEMODE_AUTO           |
        G_DIGICAM_WHITEBALANCEMODE_SUNLIGHT       |
        G_DIGICAM_WHITEBALANCEMODE_CLOUDY         |
        G_DIGICAM_WHITEBALANCEMODE_SHADE          |
        G_DIGICAM_WHITEBALANCEMODE_TUNGSTEN       |
        G_DIGICAM_WHITEBALANCEMODE_FLUORESCENT    |
        G_DIGICAM_WHITEBALANCEMODE_INCANDESCENT   |
        G_DIGICAM_WHITEBALANCEMODE_FLASH          |
        G_DIGICAM_WHITEBALANCEMODE_SUNSET;
    descriptor->supported_audio_states =
        descriptor->supported_audio_states |
        G_DIGICAM_AUDIO_RECORDON           |
        G_DIGICAM_AUDIO_RECORDOFF;
    descriptor->supported_preview_modes =
        descriptor->supported_preview_modes |
        G_DIGICAM_PREVIEW_ON |
        G_DIGICAM_PREVIEW_OFF;
    descriptor->supported_features = descriptor->supported_features |
        G_DIGICAM_CAPABILITIES_VIEWFINDER                   |
        G_DIGICAM_CAPABILITIES_RESOLUTION                   |
        G_DIGICAM_CAPABILITIES_ASPECTRATIO                  |
        G_DIGICAM_CAPABILITIES_MANUALFOCUS                  |
        G_DIGICAM_CAPABILITIES_AUTOFOCUS                    |
        G_DIGICAM_CAPABILITIES_MACROFOCUS                   |
        G_DIGICAM_CAPABILITIES_DIGITALZOOM                  |
        G_DIGICAM_CAPABILITIES_MANUALEXPOSURE               |
        G_DIGICAM_CAPABILITIES_AUTOEXPOSURE                 |
        G_DIGICAM_CAPABILITIES_AUTOWHITEBALANCE             |
        G_DIGICAM_CAPABILITIES_MANUALWHITEBALANCE           |
        G_DIGICAM_CAPABILITIES_AUTOISOSENSITIVITY           |
        G_DIGICAM_CAPABILITIES_MANUALISOSENSITIVITY         |
        G_DIGICAM_CAPABILITIES_FLASH                        |
        G_DIGICAM_CAPABILITIES_AUDIO                        |
        G_DIGICAM_CAPABILITIES_PREVIEW;
    descriptor->supported_flash_modes =
        descriptor->supported_flash_modes       |
        G_DIGICAM_FLASHMODE_OFF                 |
        G_DIGICAM_FLASHMODE_ON                  |
        G_DIGICAM_FLASHMODE_AUTO                |
        G_DIGICAM_FLASHMODE_REDEYEREDUCTION     |
        G_DIGICAM_FLASHMODE_REDEYEREDUCTIONAUTO |
        G_DIGICAM_FLASHMODE_FILLIN;
    descriptor->supported_resolutions =
        descriptor->supported_resolutions |
        G_DIGICAM_RESOLUTION_HIGH         |
        G_DIGICAM_RESOLUTION_MEDIUM       |
        G_DIGICAM_RESOLUTION_LOW;
    descriptor->supported_aspect_ratios =
        descriptor->supported_aspect_ratios |
        G_DIGICAM_ASPECTRATIO_4X3           |
        G_DIGICAM_ASPECTRATIO_16X9;
}

static void
_set_operation_mode (GDigicamMode mode)
{
    GDigicamCamerabinModeHelper *helper = NULL;
    GError *error = NULL;
    gboolean result = FALSE;

    /* FIXME: Video mode not supported yet. */
    if (mode == G_DIGICAM_MODE_VIDEO) {
        G_DIGICAM_WARN ("Video mode not supported yet.");
        return;
    }

    /* Create operation helper. */
    helper = g_slice_new (GDigicamCamerabinModeHelper);
    helper->mode = mode;

    /* Call GDigicam Manager to perform the changing mode operation. */
    result = g_digicam_manager_set_mode (digicam_manager,
					 helper->mode,
					 &error,
					 helper);
    /* Check errors */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the Operation "
                        "Mode: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Error setting the Operation "
                        "Mode: Internal error !!!");
        }
    } else {
        G_DIGICAM_DEBUG ("Operation Mode changed successfully.");
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinModeHelper,
                  helper);
}

static void
_set_white_balance (GDigicamWhitebalancemode mode)
{
    GDigicamCamerabinWhitebalanceModeHelper *helper = NULL;
    GError *error = NULL;
    gboolean result = FALSE;

    /* Create operation helper. */
    helper = g_slice_new (GDigicamCamerabinWhitebalanceModeHelper);
    helper->wb_mode = mode;

    /* Call GDigicam Manager to perform the operation. */
    result = g_digicam_manager_set_white_balance_mode (digicam_manager,
                                                       helper->wb_mode,
                                                       /* FIXME: This has been hardcoded since it is missing */
                                                       0,
                                                       &error,
                                                       helper);

    /* Check errors. */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the White Balance "
                            "Mode: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Error setting the White Balance "
                        "Mode: Internal error !!!");
        }
    } else {
        G_DIGICAM_DEBUG ("White Balance Mode changed successfully.");
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinWhitebalanceModeHelper,
                  helper);
}

static void
_set_iso_mode (gint iso_value)
{
    GDigicamCamerabinIsoSensitivityHelper *helper = NULL;
    GError *error = NULL;
    gboolean result = FALSE;

    /* Create operation helper. */
    helper = g_slice_new (GDigicamCamerabinIsoSensitivityHelper);
    helper->iso_value = iso_value;
    helper->iso_sensitivity_mode = G_DIGICAM_ISOSENSITIVITYMODE_MANUAL;
    if (iso_value == 0) {
        helper->iso_sensitivity_mode = G_DIGICAM_ISOSENSITIVITYMODE_AUTO;
    }

    /* Call GDigicam Manager to perform the operation. */
    result = g_digicam_manager_set_iso_sensitivity_mode (digicam_manager,
                                                         helper->iso_sensitivity_mode,
                                                         helper->iso_value,
                                                         &error,
                                                         helper);

    /* Check errors. */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the ISO "
                            "Mode: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Error setting the White Balance "
                            "Mode: Internal error !!!");
        }
    } else {
        G_DIGICAM_DEBUG ("White Balance Mode changed successfully.");
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinIsoSensitivityHelper,
                  helper);
}

static void
_set_flash_mode (GDigicamFlashmode mode)
{
    GDigicamCamerabinFlashModeHelper *helper = NULL;
    GError *error = NULL;
    gboolean result = FALSE;

    /* Create operation helper. */
    helper = g_slice_new (GDigicamCamerabinFlashModeHelper);
    helper->flash_mode = mode;

    /* Call GDigicam Manager to perform the operation. */
    result = g_digicam_manager_set_flash_mode (digicam_manager,
                                               helper->flash_mode,
                                               &error,
                                               helper);

    /* Check errors. */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the Flash "
                            "Mode: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Error setting the Flash "
                            "Mode: Internal error !!!");
        }
    } else {
        G_DIGICAM_DEBUG ("Flash Mode changed successfully.");
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinFlashModeHelper,
                  helper);
}

static void
_set_zoom_value (gdouble value)
{
    GDigicamCamerabinZoomHelper *helper = NULL;
    gboolean digital = FALSE;
    GError *error = NULL;
    gboolean result = FALSE;

    /* Create operation helper. */
    helper = g_slice_new (GDigicamCamerabinZoomHelper);
    helper->value = value;

    /* Call GDigicam Manager to perform the operation. */
    result = g_digicam_manager_set_zoom (digicam_manager,
                                         helper->value,
                                         &digital,
                                         &error,
                                         helper);

    /* Check errors. */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the Zoom "
                            "Value: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Error setting the Zoom "
                            "Value: Internal error !!!");
        }
    } else {
        G_DIGICAM_DEBUG ("Zoom Value changed successfully.");
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinZoomHelper,
                  helper);
}

static void
_enable_preview (gboolean enable)
{
    GDigicamCamerabinPreviewHelper * helper = NULL;
    GError *error = NULL;
    gboolean result = FALSE;

    /* Create operation helper. */
    helper = g_slice_new (GDigicamCamerabinPreviewHelper);
    if (enable) {
        helper->mode = G_DIGICAM_PREVIEW_ON;
    } else {
        helper->mode = G_DIGICAM_PREVIEW_OFF;
    }

    /* Call GDigicam Manager to perform the operation. */
    result = g_digicam_manager_set_preview_mode (digicam_manager,
                                                 helper->mode,
                                                 &error,
                                                 helper);

    /* Check errors. */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the Preview "
                            "Mode: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Error setting the Preview "
                            "Mode: Internal error !!!");
        }
    } else {
        G_DIGICAM_DEBUG ("Preview Mode changed successfully.");
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinPreviewHelper,
                  helper);
}

/* static void */
/* _set_audio (GDigicamAudio mode) */
/* { */
/*     GDigicamCamerabinVideoHelper * helper = NULL; */
/*     GError *error = NULL; */
/*     gboolean result = FALSE; */

/*     /\* Create operation helper. *\/ */
/*     helper = g_slice_new (GDigicamCamerabinVideoHelper); */
/*     helper->audio = mode; */

/*     /\* Call GDigicam Manager to perform the operation. *\/ */
/*     result = g_digicam_manager_set_audio (digicam_manager, */
/*                                           helper->audio, */
/*                                           &error, */
/*                                           helper); */

/*     /\* Check errors. *\/ */
/*     if (!result) { */
/*         if (error != NULL) { */
/*             G_DIGICAM_DEBUG ("Error setting the Audio " */
/*                             "Mode: %s", error->message); */
/*             g_error_free (error); */
/*         } else { */
/*             G_DIGICAM_DEBUG ("Error setting the Audio " */
/*                             "Mode: Internal error !!!"); */
/*         } */
/*     } else { */
/*         G_DIGICAM_DEBUG ("Audio Mode changed successfully."); */
/*     } */

/*     /\* Free. *\/ */
/*     g_slice_free (GDigicamCamerabinVideoHelper, */
/*                   helper); */
/* } */

static void
_set_exposure_comp (gdouble value)
{
    GDigicamCamerabinExposureCompHelper *helper = NULL;
    GError *error = NULL;
    gboolean result = FALSE;

    /* Create operation helper. */
    helper = g_slice_new (GDigicamCamerabinExposureCompHelper);
    helper->exposure_comp = value;

    /* Call GDigicam Manager to perform the operation. */
    result = g_digicam_manager_set_exposure_comp (digicam_manager,
						  helper->exposure_comp,
						  &error,
						  helper);

    /* Check errors. */
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the Exposure Compensation "
                            "Value: %s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Error setting the Exposure Compensation "
                            "Value: Internal error !!!");
        }
    } else {
        G_DIGICAM_DEBUG ("Exposure Compensation Value changed successfully.");
    }

    /* Free. */
    g_slice_free (GDigicamCamerabinExposureCompHelper,
                  helper);
}


static GDigicamExposureconf *
_get_exposure_mode_specific_conf (GDigicamExposuremode mode)
{
    GDigicamExposureconf *conf = NULL;

    conf = g_slice_new0 (GDigicamExposureconf);

    /* Get exposure mode specific settings. */
    switch (mode) {
    case G_DIGICAM_EXPOSUREMODE_AUTO:
        conf->iso_sensitivity_mode = G_DIGICAM_ISOSENSITIVITYMODE_AUTO;
        conf->white_balance_mode = G_DIGICAM_WHITEBALANCEMODE_AUTO;
        conf->white_balance_level = 0;
        conf->flash_mode = G_DIGICAM_FLASHMODE_AUTO;
        conf->exposure_compensation = 0;
        break;
    case G_DIGICAM_EXPOSUREMODE_LANDSCAPE:
        conf->iso_sensitivity_mode = G_DIGICAM_ISOSENSITIVITYMODE_AUTO;
        conf->white_balance_mode = G_DIGICAM_WHITEBALANCEMODE_AUTO;
        conf->white_balance_level = 0;
        conf->flash_mode = G_DIGICAM_FLASHMODE_OFF;
        conf->exposure_compensation = 0;
        break;
    case G_DIGICAM_EXPOSUREMODE_NIGHT:
        conf->iso_sensitivity_mode = G_DIGICAM_ISOSENSITIVITYMODE_AUTO;
        conf->white_balance_mode = G_DIGICAM_WHITEBALANCEMODE_AUTO;
        conf->white_balance_level = 0;
        conf->flash_mode = G_DIGICAM_FLASHMODE_AUTO;
        conf->exposure_compensation = 0;
        break;
    case G_DIGICAM_EXPOSUREMODE_PORTRAIT:
        conf->iso_sensitivity_mode = G_DIGICAM_ISOSENSITIVITYMODE_AUTO;
        conf->white_balance_mode = G_DIGICAM_WHITEBALANCEMODE_AUTO;
        conf->white_balance_level = 0;
        conf->flash_mode = G_DIGICAM_FLASHMODE_AUTO;
        conf->exposure_compensation = 0;
        break;
    case G_DIGICAM_EXPOSUREMODE_SPORTS:
        conf->iso_sensitivity_mode = G_DIGICAM_ISOSENSITIVITYMODE_AUTO;
        conf->white_balance_mode = G_DIGICAM_WHITEBALANCEMODE_AUTO;
        conf->white_balance_level = 0;
        conf->flash_mode = G_DIGICAM_FLASHMODE_OFF;
        conf->exposure_compensation = 0;
        break;
    default:
        g_assert_not_reached ();
    }

    return conf;
}

static void
_set_exposure_mode (GDigicamExposuremode exp_mode,
                    gboolean macro_enabled)
{
    GDigicamCamerabinFocusModeHelper *focus_helper = NULL;
    GDigicamCamerabinExposureModeHelper *exposure_helper = NULL;
    GDigicamExposureconf *conf = NULL;
    GError *error = NULL;
    gboolean result = FALSE;

    /* Create helpers. */
    exposure_helper = g_slice_new (GDigicamCamerabinExposureModeHelper);
    exposure_helper->exposure_mode = exp_mode;
    focus_helper = g_slice_new (GDigicamCamerabinFocusModeHelper);
    focus_helper->macro_enabled = FALSE;
    focus_helper->focus_mode = G_DIGICAM_FOCUSMODE_AUTO;
    focus_helper->macro_enabled = macro_enabled;

    /* Get ExposureMode specific configuration parameters. */
    conf = _get_exposure_mode_specific_conf (exp_mode);

    /* Set focus mode to AUTO (default when changing exposure mode). */
    result = g_digicam_manager_set_focus_mode (digicam_manager,
                                               focus_helper->focus_mode,
                                               focus_helper->macro_enabled,
                                               &error,
                                               focus_helper);
    if (!result) {
        goto error;
    }

    /* Call GDigicam Manager to perform the changing exposure mode
     * operation. */
    result = g_digicam_manager_set_exposure_mode (digicam_manager,
                                                  exposure_helper->exposure_mode,
                                                  conf,
                                                  &error,
                                                  exposure_helper);
    if (!result) {
        goto error;
    }

    /* Check errors. */
error:
    if (!result) {
        if (error != NULL) {
            G_DIGICAM_DEBUG ("Error setting the exposure mode: "
                             "%s", error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Internal error !!!");
        }
    }

    g_slice_free (GDigicamExposureconf, conf);
    g_slice_free (GDigicamCamerabinFocusModeHelper, focus_helper);
    g_slice_free (GDigicamCamerabinExposureModeHelper, exposure_helper);
}


static void
_set_resolution (GDigicamResolution res,
                 GDigicamAspectratio ar)
{
    GDigicamCamerabinAspectRatioResolutionHelper *helper = NULL;
    GError *error = NULL;
    gboolean result;


    G_DIGICAM_DEBUG ("Setting new resolution %d...", res);

    /* Create helper */
    helper = g_slice_new (GDigicamCamerabinAspectRatioResolutionHelper);
    helper->resolution = res;
    helper->aspect_ratio = ar;

    /* Call GDigicam Manager to perform the operation. */
    result = g_digicam_manager_set_aspect_ratio_resolution (digicam_manager,
							    helper->aspect_ratio,
							    helper->resolution,
							    &error,
							    helper);
    if (!result) {
        goto error;
    }

    /* Check errors. */
error:
    if (!result) {
        if (NULL != error) {
            G_DIGICAM_DEBUG ("Error setting resolution: "
                        "%s", error->message);

            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Internal error !!!");
        }
    }

    g_slice_free (GDigicamCamerabinAspectRatioResolutionHelper, helper);
}


static void
_default_testing_benchmark ()
{
    benchConf = g_slice_new0 (BenchmarkConf);
    benchConf->set1 = DISABLED;
    benchConf->set2 = DISABLED;
    benchConf->set3 = DISABLED;
    benchConf->set4 = DISABLED;
    benchConf->set5 = DISABLED;
    benchConf->set6 = DISABLED;
}

static void
_load_default_settings ()
{
    dcamConf = g_slice_new0 (GDigicamConf);

    dcamConf = g_slice_new0 (GDigicamConf);
    dcamConf->mode     = G_DIGICAM_MODE_STILL;
    dcamConf->res      = G_DIGICAM_RESOLUTION_HIGH;
    dcamConf->ar       = G_DIGICAM_ASPECTRATIO_16X9;
    dcamConf->flash    = G_DIGICAM_FLASHMODE_AUTO;
    dcamConf->iso      = 0;
    dcamConf->wb       = G_DIGICAM_WHITEBALANCEMODE_AUTO;
    dcamConf->exp      = G_DIGICAM_EXPOSUREMODE_AUTO;
    dcamConf->exp_comp = 0;
    dcamConf->preview  = TRUE;
    dcamConf->macro    = FALSE;
    dcamConf->zoom     = 1;

    /* FIXME: implement here your custom default configuration. */
    _load_settings ();
}

static void
_load_settings (void)
{
    _set_operation_mode (dcamConf->mode);
    _set_resolution     (dcamConf->res, dcamConf->ar);
    _set_exposure_mode  (dcamConf->exp, dcamConf->macro);
    _set_exposure_comp  (dcamConf->exp_comp);
    _set_white_balance  (dcamConf->wb);
    _set_flash_mode     (dcamConf->flash);
    _set_iso_mode       (dcamConf->iso);
    _enable_preview     (dcamConf->preview);
    _set_zoom_value     (dcamConf->zoom);
}

static void
_set_new_camerabin (const gchar *venc)
{
    GDigicamDescriptor *descriptor = NULL;
    GstElement *bin = NULL;
    GError *error = NULL;

    /* Build a new Camerabin component. */
    bin = _build_camerabin_component (venc);

    /* FIXME: Custom descriptor. GDigicam could try to */
    /* detect the camera capabilities. */
    descriptor = g_digicam_camerabin_descriptor_new (bin);
    _descriptor_set_capabilities (descriptor);

    /* Set the camerabin into the GDigicamManager instance. */
    if (!g_digicam_manager_set_gstreamer_bin (digicam_manager,
                                              bin,
                                              descriptor,
                                              &error)) {
        if (error != NULL) {
            G_DIGICAM_ERR("Error setting bin: %s",
                            error->message);
        } else {
            G_DIGICAM_ERR("Error setting bin: Internal error !!!");
        }
    }
}

static void
_setup_gdigicam_manager ()
{

    G_DIGICAM_DEBUG ("TestApp::_setup_gdigicam_manager: "
                     "Setting GDigicam Manager.");

    /* Create the GDigicamManager instance. */
    digicam_manager = g_digicam_manager_new ();
    if (NULL == digicam_manager) {
        G_DIGICAM_DEBUG ("TestApp::_cam_facade_gdigicam_new: "
                    "GDigicamManager creation failed !!!");
    }

    /* Building and set a camerabin component into the */
    /* GDigicamManager instace. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    _set_new_camerabin ("dspmp4venc");
#else
    _set_new_camerabin ("jpegenc");
#endif

    /* Set device configuration parameters. */
    _load_default_settings ();

    /* Set default benchmarking sets. */
    _default_testing_benchmark ();
}

static GstElement *
_build_camerabin_component (const gchar *videoenc)
{
    GstElement *bin = NULL;
    gboolean ok = TRUE;
    GError *error = NULL;

    G_DIGICAM_DEBUG ("Setting camerabin.");

    /* Create a new instance of Camerabin component. */
#ifdef GDIGICAM_PLATFORM_MAEMO
    bin = g_digicam_camerabin_element_new ("v4l2camsrc",
                                           videoenc,
                                           "hantromp4mux",
                                           "pulsesrc",
                                           "nokiaaacenc",
                                           "jpegenc",
                                           NULL,
                                           "xvimagesink",
                                           &colorkey);
#else
    bin = g_digicam_camerabin_element_new ("v4l2src",
                                           videoenc,
                                           "qtmux",
                                           "gconfaudiosrc",
                                           "ffenc_mp2",
                                           "jpegenc",
                                           NULL,
                                           "xvimagesink",
                                           &colorkey);
#endif

    /* Check errors. */
    if (NULL == bin) {
        ok = FALSE;
        G_DIGICAM_DEBUG ("'camerabin' GStreamer bin creation failed !!!");
        goto cleanup;
    }

cleanup:
    if (!ok) {
        if (NULL != error) {
            G_DIGICAM_DEBUG ("ERROR: %s",
                             error->message);
            g_error_free (error);
        } else {
            G_DIGICAM_DEBUG ("Internal error !!!");
        }
    }

    return bin;
}



static void
_on_capture_start (GDigicamManager *digicam_manager,
                   gpointer data)
{
    G_DIGICAM_DEBUG ("GDigicamTest::_on_capture_start: "
                     "'capture-start' signal received");

    ready_for_new_capture = FALSE;
#ifdef GDIGICAM_PLATFORM_MAEMO
    hildon_banner_show_information (GTK_WIDGET (data),
                                    NULL,
                                    "Capture operation started.");
#endif

    return;
}

static void
_on_picture_got (GDigicamManager *digicam_manager,
                 gpointer data)
{
    G_DIGICAM_DEBUG ("GDigicamTest::_on_picture_got: "
                     "'picture-got' signal received");

#ifdef GDIGICAM_PLATFORM_MAEMO
    hildon_banner_show_information (GTK_WIDGET (data),
                                    NULL,
                                    "Image capture initied.");
#endif
}

static void
_on_capture_end (GDigicamManager *digicam_manager,
                 gpointer data)
{
    G_DIGICAM_DEBUG ("GDigicamTest::_on_capture_end: "
                     "'capture-end' signal received");

    ready_for_new_capture = TRUE;
#ifdef GDIGICAM_PLATFORM_MAEMO
    hildon_banner_show_information (GTK_WIDGET (data),
                                    NULL,
                                    "Image capture completed.");
#endif
}


static void
_on_image_preview (GDigicamManager *digicam_manager,
                   GdkPixbuf *preview,
                   gpointer data)
{
    G_DIGICAM_DEBUG ("GDigicamTest::_on_image_preview: "
                     "'image-preview' signal received");

#ifdef GDIGICAM_PLATFORM_MAEMO
    hildon_banner_show_information (GTK_WIDGET (data),
                                    NULL,
                                    "Preview generated.");
#endif
}

static gboolean
_on_image_capture_done (GDigicamManager *digicam_manager,
                        const gchar *fname,
                        gpointer data)
{
    G_DIGICAM_DEBUG ("GDigicamTest::_on_image_capture_done: "
                     "'pict-done' signal received");

#ifdef GDIGICAM_PLATFORM_MAEMO
    hildon_banner_show_information (GTK_WIDGET (data),
                                    NULL,
                                    "Image saved on memory.");
#endif
    _benchmark_set1_test1 ();

    return FALSE;
}

static void
_on_internal_error (GDigicamManager *digicam_manager,
                    gpointer data)
{
    G_DIGICAM_DEBUG ("GDigicamTest::_on_internal_error: "
                     "'internal-error' signal received");

#ifdef GDIGICAM_PLATFORM_MAEMO
    hildon_banner_show_information (GTK_WIDGET (data),
                                    NULL,
                                    "Internal error !!!");
#endif

    /* Do no perform additional iterations. */
    test_iter = MAX_ITER;
}
